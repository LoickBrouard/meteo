const baseUrl = "http://172.20.10.2/";
var temp = [];
var hum = [];
var dateTemp = [];
var dateHum = [];
var refreshRateTemp = 1;
var refreshRateHum = 1;


function refresh(nombre){
    refreshRateTemp = nombre
}

function refreshHumidite(nombre) {
    refreshRateHum = nombre
}

function dateConverter(date) {
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}

function GetTempFromAPI(numberOfRecords) {
    temp = [];
    dateTemp = [];
    $.ajax({
        method: 'POST',
        url: baseUrl + "GetInfos",
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        dataType: 'json',
        data: { nombre: numberOfRecords },
        success: function (data) {
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i]["ID_DATAS"] % refreshRateTemp == 0) {
                        temp.push(data[i]["DATA_TEMPERATURE"]);
                        dateTemp.push(new Date(data[i]["DATA_DATE"]));
                    }
                };
                LoadChartTemp(dateTemp, temp);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    })
}

function GetHumFromAPI(numberOfRecords) {
    hum = [];
    dateHum = [];
    $.ajax({
        method: 'POST',
        url: baseUrl + "GetInfos",
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        dataType: 'json',
        data: { nombre: numberOfRecords },
        success: function (data) {
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    console.log(`refreshRateHum : ${refreshRateHum}`);
                    console.log(`data[i]["ID_DATAS"] : ${data[i]["ID_DATAS"]}`);
                    console.log(`Reste de la division : ${data[i]["ID_DATAS"] % refreshRateHum}`);
                    if (data[i]["ID_DATAS"] % refreshRateHum == 0) {
                        console.log(data[i]["DATA_HUMIDITE"]);
                        hum.push(data[i]["DATA_HUMIDITE"]);
                        dateHum.push(new Date(data[i]["DATA_DATE"]));
                    }
                };
                LoadChartHum(dateHum, hum);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    })
}

function lastInfo(numberOfRecords) {
    $.ajax({
        method: 'POST',
        url: baseUrl + "GetInfos",
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        dataType: 'json',
        data: { nombre: numberOfRecords },
        success: function (data) {
            var t = document.getElementById('last-rec-temp');
            var h = document.getElementById('last-rec-hum');
            var dt = document.getElementById('last-rec-time-temp');
            var dh = document.getElementById('last-rec-time-hum');

            t.innerHTML = `${data[0]["DATA_TEMPERATURE"]} °C`;
            h.innerHTML = `${data[0]["DATA_HUMIDITE"]} %`;
            dt.innerHTML = dateConverter(new Date(data[0]["DATA_DATE"]));
            dh.innerHTML = dateConverter(new Date(data[0]["DATA_DATE"]));
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    })
}

function LoadChartTemp(dateTemp, temp) {
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Weekly Average Temperature'
        },
        subtitle: {
            text: 'Source: notre super sonde'
        },
        xAxis: {
            categories: dateTemp,
            reversed: true,
            type: 'datetime',
            timezone: 'Europe/Paris',
            labels: {
                formatter: function () {
                    return Highcharts.dateFormat('%H:%M:%S',
                        this.value);
                }
            }
        },
        yAxis:
        {
            title:
            {
                text: 'Temperature (°C)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true

            },
            series: {
                animation: false
            }
        },
        series: [{
            name: 'sonde 1',
            data: temp
        }]
    });
}

function LoadChartHum(dateHum, hum) {
    Highcharts.chart('container2', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Weekly Average Humidity'
        },
        subtitle: {
            text: 'Source: notre super sonde'
        },
        xAxis: {
            categories: dateHum,
            reversed: true,
            type: 'datetime',
            timezone: 'Europe/Paris',
            labels: {
                formatter: function () {
                    return Highcharts.dateFormat('%H:%M:%S',
                        this.value);
                }
            }
        },
        yAxis:
        {
            title:
            {
                text: 'Humidité (%)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            },
            series: {
                animation: false
            }
        },
        series: [{
            name: 'sonde 1',
            data: hum
        }]
    });
}

// window.onload = setupRefresh;

// function setupRefresh() {
//   setInterval("refreshFrame();", 1000);
// }
// function refreshFrame() {
//   var loc = document.getElementById('temperature');
//   loc.reload();
// }

function updateDiv() {
    $("#temperature").load(window.location.href + " #temperature");
}

GetTempFromAPI(20 * refreshRateTemp);
GetHumFromAPI(20 * refreshRateHum);
setInterval(() => {
    GetHumFromAPI(20 * refreshRateHum);
    GetTempFromAPI(20 * refreshRateTemp);
    lastInfo(1);
    temperature();
}, 3000);


function temperature() {
    var temp = document.getElementById('last-rec-temp');
    var lasttemp = parseInt(temp.innerHTML);
    var hum = document.getElementById('last-rec-hum');
    var lasthum = parseInt(hum.innerHTML);

    if ( lasttemp <= 10 ) {
        temp.style.color = "#CAF6FF";
    } 
    if ( lasttemp > 10 && lasttemp <= 20 ) {
        temp.style.color = "#2ADBFF";
    }
    if ( lasttemp > 20 & lasttemp <= 30) {
        temp.style.color = "#FFA047";
    }
    if (lasttemp > 30 & lasttemp <= 40) {
        temp.style.color = "#FE771A";
    }
    if (lasttemp > 40) {
        temp.style.color = "#FF0000";
    }

    if (lasthum <= 20) {
        hum.style.color = "#FFB8ED";
    }
    if (lasthum > 20 & lasthum <= 40) {
        hum.style.color = "#FF70DB";
    }
    if (lasthum > 40 & lasthum <= 60) {
        hum.style.color = "#B80089";
    }
    if (lasthum > 60 & lasthum <= 80) {
        hum.style.color = "#4B46B0";
    }
    if (lasthum > 80 & lasthum <= 100) {
        hum.style.color = "#0800A1";
    }
}

