// Librairies
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Adafruit_HTU21DF.h> // Comm. capteur
#include <LiquidCrystal_I2C.h> // Comm. screen
#include <ArduinoJson.h> // Json handling

#if defined(ARDUINO) && ARDUINO >= 100
#define printByte(args)  write(args);
#else
#define printByte(args)  print(args,BYTE);
#endif

// Info wifi
const char* ssid = "Loïck";           // WiFi id
const char* password = "azertyuio";  // WiFi password

// Infos Capteur
const String latitude = "49.38173668148508";
const String mdp = "12345abc";
const String longitude = "1.0739455034193934";
const String altitude = "56";
const String nomCapteur = "CapteurCesi";

// Character loading
uint8_t oneLine[8] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x1f};
uint8_t twoLines[8] = {0x0,0x0,0x0,0x0,0x0,0x0,0x1f,0x1f};
uint8_t threeLines[8] = {0x0,0x0,0x0,0x0,0x0,0x1f,0x1f,0x1f};
uint8_t fourLines[8] = {0x0,0x0,0x0,0x0,0x1f,0x1f,0x1f,0x1f};
uint8_t fiveLines[8] = {0x0,0x0,0x0,0x1f,0x1f,0x1f,0x1f,0x1f};
uint8_t sixLines[8] = {0x0,0x0,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f};
uint8_t sevenLines[8] = {0x0,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f};
uint8_t eightLines[8] = {0x1f,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f,0x1f};

// Instantiate capt. reader 
Adafruit_HTU21DF htu = Adafruit_HTU21DF(); 

// Instantiate lcd
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Instanciate Json
StaticJsonDocument<1000> doc;

void affichEcran(String messageLigne1="", String messageLigne2="")
{
 lcd.clear();
 lcd.setCursor(0, 0);
 lcd.print(messageLigne1);
 lcd.setCursor(0, 1);
 lcd.print(messageLigne2);
}

void loadInfosAwaiter(String messageLigne1="", String messageLigne2="", int delayTime=5000)
{
  int timeFract = round(delayTime/8);
  int CurrTime = 0;
  int i = 0;
  while(CurrTime < delayTime)
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(messageLigne1);
    lcd.setCursor(0, 1);
    lcd.print(messageLigne2);
    
    lcd.setCursor(15, 0);
    lcd.write(byte(i));
    i ++; 
    delay(timeFract);
    CurrTime += timeFract;
  }
}

void setup() {
  // HTU21 CONNECTION
  Wire.begin(0,2); // Initialize I2C LCD module (SDA = GPIO0, SCL = GPIO2)
  
  // LCD INSTANCE
  lcd.begin(0,2); // Initialize I2C LCD module (SDA = GPIO0, SCL = GPIO2)
  lcd.init();
  lcd.backlight();
  affichEcran("Bonjour !", "");
  delay(5000);

  lcd.createChar(0, oneLine);
  lcd.createChar(1, twoLines);
  lcd.createChar(2, threeLines);
  lcd.createChar(3, fourLines);
  lcd.createChar(4, fiveLines);
  lcd.createChar(5, sixLines);
  lcd.createChar(6, sevenLines);
  lcd.createChar(7, eightLines);
  
  // WIFI CONNECTION
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  affichEcran("Connecting to ", "wifi");
  Serial.println(ssid);
  String awaiter = ".";
  
  while (WiFi.status() != WL_CONNECTED) 
    {
     affichEcran("Connecting to ", "wifi" + awaiter);
     awaiter += ".";
     delay(1000);
     if(String(ssid).length() + awaiter.length() == 16){
      awaiter = "";
     }
    } 
  
  Serial.println("");
  Serial.println("Connected");
  affichEcran("Connected", "");



}      
void loop() {
  // WIFI CONDITION
  if (WiFi.status() == WL_CONNECTED) 
  {
    
    String temp = String(htu.readTemperature());
    String hum = String(htu.readHumidity());
    String infoMeteoLine1 = "Deg.C : " + temp;
    String infoMeteoLine2 = "Humidite: " + hum;
    doc["temperature"] = temp;
    doc["humidite"] =  hum;
    doc["motdepasse"] = mdp;
    doc["latitude"] = latitude;
    doc["longitude"] = longitude;
    doc["altitude"] = altitude;
    doc["nomcapteur"] = nomCapteur;

    
    String contentPost = "";
    serializeJson(doc, contentPost);
    
    HTTPClient http; //Object of class HTTPClient
    http.begin("http://172.20.10.2/capteur");
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Content-Length", "100000");
    int httpCode = http.POST(contentPost);
    
    Serial.println(contentPost);
    Serial.print("HTTP response code: ");
    Serial.println(httpCode);
    
    switch(httpCode){
      case 404:
        affichEcran("Serveur",  "non trouve");
        break;
      case 403:
        affichEcran("Accès serveur",  "refuse");
        break;
      case 500:
        affichEcran("Serveur",  "en erreur");
        break;
       case 503:
        affichEcran("Serveur",  "en erreur");
        break;
       case 0:
        affichEcran("Serveur",  "en erreur");
        break;
       case -1:
        affichEcran("Serveur",  "en erreur");
        break;
      default:
        affichEcran("Valeurs",  "Envoyees");
        break;
    }
    delay(5000);
    http.end(); //Close connection

    loadInfosAwaiter(infoMeteoLine1, infoMeteoLine2, 25000);
  }
  else{
    affichEcran("Wifi","disconnected");
  }
}
